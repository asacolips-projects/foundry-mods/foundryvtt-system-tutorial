# FoundryVTT System Tutorial

- Getting an empty system together
    - Start with SWS
    - Start with a boilerplate system
    - Using a CLI
- Stuff to be aware of
    - Scripts or ES6 modules?
    - CSS preprocessors
    - Handlebars
    - What about 3rd party libraries?
    - Localization
- system.json
- template.json
    - Defining actor and item types
    - Defining common data templates
- Overriding actors
- Overriding actor sheets
    - Creating new tabs for different kinds of items
- Creating derived data for actors
    - Basic data vs derived data, and where should it go?
    - Actor data structure
    - Sorting and grouping items
    - Creating item-like data structures
    - Querying for entities with map and filter
    - When in doubt, look at other systems
- Overriding items
- Overriding item sheets
- Creating rollable buttons with event listeners
- Creating rolls that require user inputs
- CONFIG and things to do with it
- More Localization
- TabsV2
- Making things draggable in your sheet
- Adding macrobar support for your items
- Overriding core behaviors (like the combat tracker)
- Development patterns to be aware of in the API
<!--stackedit_data:
eyJoaXN0b3J5IjpbNTQzOTcyMTQyXX0=
-->